<?php

namespace Drupal\podlove_player\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\podlove_player\Feed;

/**
 * Plugin implementation of the 'Random_default' formatter.
 *
 * @FieldFormatter(
 *   id = "podlove_player",
 *   label = @Translation("Podlove Player"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class PodlovePlayerFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Load the linked RSS feed in the Podlove player.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      try {
        $feed = new Feed($item->getUrl());
      }
      catch (\InvalidArgumentException $e) {
        $element[$delta] = ['#markup' => 'Failed to load RSS feed.'];
        /* TODO: Log the exceptions message. */
        continue;
      }

      $episode_settings = $this->getEpisodeSettings($feed);

      $id = Html::getUniqueId('podlove_player_' . $delta);
      $element[$delta] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => '',
        '#attributes' => ['id' => $id],
        '#attached' => [
          'library' => ['podlove_player/podlove-player'],
          'drupalSettings' => [
            'podlovePlayer' => [
              'podlovePlayer' => [
                $delta => [
                  'selector' => '#' . $id,
                  'episode' => $episode_settings,
                  'config' => $this->getPlayerConfiguration($feed),
                ],
              ],
            ],
          ],
        ],
        /* Provide the feed URL, so that other modules that preprocess the
         * render array can use it. */
        '#feed_url' => $item->getUrl(),
      ];
    }

    return $element;
  }

  /**
   * Get the settings array for an episode that should be played in the player.
   *
   * @param \Drupal\podlove_player\Feed $feed
   *   The feed object from which to read the data.
   * @param int $index
   *   The index of the item for which to return the configuration.
   *
   * @return array
   *   The episode configuration that can be passed to the Podlove player.
   */
  protected function getEpisodeSettings(Feed $feed, int $index = 0) {
    $settings = [
      'version' => 5,
    ];

    $settings['show'] = [
      'title' => $feed->getTitle(),
      'subtitle' => $feed->getSubtitle(),
      'summary' => $feed->getDescription(),
      'poster' => $feed->getImageUrl(),
      'link' => $feed->getLink(),
    ];

    /* TODO: The episode configuration has an additional field 'poster' for the
     * episode's image's URI. We might want to support that as well. */
    $settings['title'] = $feed->getItemTitle($index);
    $settings['subtitle'] = '';
    $settings['summary'] = $feed->getItemDescription($index);
    $settings['publicationDate'] = $feed->getItemPublicationDate($index);
    $settings['duration'] = $feed->getItemDuration($index);
    $settings['link'] = $feed->getItemLink($index);

    /* TODO: An audio item also takes a 'title' argument. */
    /* TODO: Do we need support for multiple audio items? */
    $settings['audio'][] = [
      'url' => $feed->getItemAudioUri($index),
      'size' => $feed->getItemAudioSize($index),
      'mimeType' => $feed->getItemAudioType($index),
    ];

    return $settings;
  }

  /**
   * Get the settings array for the player configuration.
   *
   * @return array
   *   The player's configuration.
   */
  protected function getPlayerConfiguration($feed) {
    $settings = [
      'version' => 5,
      'activeTab' => 'playlist',
      'playlist' => [],
    ];

    $item_count = $feed->getItemCount();
    for ($i = 0; $i < $item_count; $i++) {
      $settings['playlist'][] = [
        'title' => $feed->getItemTitle($i),
        'config' => $this->getEpisodeSettings($feed, $i),
        'duration' => $feed->getItemDuration($i),
      ];
    }
    return $settings;
  }

}
