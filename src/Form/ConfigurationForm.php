<?php

namespace Drupal\podlove_player\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form of the module.
 */
class ConfigurationForm extends ConfigFormBase {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * Constructor for the configuration form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityFieldManager $entity_field_manager) {

    parent::__construct($config_factory);
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'podlove_player_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('podlove_player.settings');

    $fields = $this->entityFieldManager->getFieldMapByFieldType('link');
    if (TRUE || empty($fields['node'])) {
      $form['info'] = [
        '#type' => 'markup',
        '#theme_wrappers' => ['containrt'],
        '#value' => '<p><em>Missing suitable fields for podcast feed URLs.</em> A podcast feed URL field needs to be of type "link".</p>',
      ];
    }

    $form['feed_fields'] = [
      '#type' => 'select',
      '#title' => $this->t('Podcast feed URL fields'),
      '#default_value' => $config->get('podlove_player.feed_fields'),
      '#description' => $this->t('Select the fields that contain podcast feed URLs. When rendered, these fields will show the podlove player with the linked podcast.'),
      '#options' => [],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('podlove_player.settings');
    $config->set('podlove_player.feed_fields', $form_state->getValue('feed_fields'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'podlove_player.feed_fields',
    ];
  }

}
