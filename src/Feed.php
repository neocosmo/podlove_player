<?php

namespace Drupal\podlove_player;

use Drupal\Core\Url;

/**
 * An RSS feed.
 */
class Feed {
  /**
   * The URL of the RSS feed.
   *
   * @var \Drupal\Core\Url
   */
  protected $url;

  /**
   * The version of the RSS feed.
   *
   * @var string
   */
  protected $version;

  /**
   * The channel data from the RSS feed.
   *
   * @var \SimpleXMLElement
   */
  protected $channel;

  /**
   * The channels items.
   *
   * @var \SimpleXMLElement[]
   */
  protected $items;

  /**
   * The namespaces used in the RSS feed.
   *
   * @var string[]
   */
  protected $namespaces;

  /**
   * Construct a new feed from a URI.
   *
   * @param \Drupal\Core\Url $url
   *   The URI of the RSS feed.
   */
  public function __construct(Url $url) {
    $this->url = $url;

    $xml = file_get_contents($url->setAbsolute()->toString());

    try {
      $feed = new \SimpleXMLElement($xml);
    }
    catch (Exception $e) {
      throw new \InvalidArgumentException($e->message);
    }

    $this->version = (string) $feed['version'];
    $this->channel = $feed->channel;

    $this->namespaces = $feed->channel->getNamespaces(TRUE);

    foreach ($feed->channel->children() as $key => $item) {
      if ($key !== 'item') {
        continue;
      }
      $this->items[] = $item;
    }

    if (empty($this->channel)) {
      throw new \InvalidArgumentException('Invalid feed data. Missing channel information.');
    }

    if ($this->version !== '2.0') {
      throw new \InvalidArgumentException('Feed has an unsupported version.');
    }
  }

  /**
   * Get the feed's title.
   *
   * @return string
   *   The feed's title or an empty string.
   */
  public function getTitle() {
    return (string) $this->channel->title ?? '';
  }

  /**
   * Get the feed's subtitle.
   *
   * @return string
   *   The feed's subtitle or an empty string.
   */
  public function getSubtitle() {
    if (empty($this->namespaces['itunes'])) {
      return '';
    }
    return (string) $this->channel->children($this->namespaces['itunes'])->subtitle ?? '';
  }

  /**
   * Get the feeds description.
   *
   * @return string
   *   The feed's description or an empty string.
   */
  public function getDescription() {
    return (string) $this->channel->description ?? '';
  }

  /**
   * Get the feed's image's URI.
   *
   * @return string
   *   The URI of the feed's image or an empty string.
   */
  public function getImageUrl() {
    if (empty($this->channel->image)) {
      return '';
    }
    return (string) $this->channel->image->url ?? '';
  }

  /**
   * Get the feed's link.
   *
   * @return string
   *   The feed's link or an empty string.
   */
  public function getLink() {
    return (string) $this->channel->link ?? '';
  }

  /**
   * Get the number of items in the feed.
   *
   * @return int
   *   The number of items of the feed.
   */
  public function getItemCount() {
    return count($this->items);
  }

  /**
   * Get an item's title.
   *
   * @param int $index
   *   The item's index.
   *
   * @return string
   *   The title of the item or an empty string.
   */
  public function getItemTitle(int $index) {
    if (empty($this->items[$index])) {
      return '';
    }
    return (string) $this->items[$index]->title ?? '';
  }

  /**
   * Get an item's desciption.
   *
   * @param int $index
   *   The item's index.
   *
   * @return string
   *   The description of the item or an empty string.
   */
  public function getItemDescription(int $index) {
    if (empty($this->items[$index])) {
      return '';
    }
    return (string) $this->items[$index]->description ?? '';
  }

  /**
   * Get an item's link.
   *
   * @param int $index
   *   The item's index.
   *
   * @return string
   *   The link of the item or an empty string.
   */
  public function getItemLink(int $index) {
    if (empty($this->items[$index])) {
      return '';
    }
    return (string) $this->items[$index]->link ?? '';
  }

  /**
   * Get an item's publication date.
   *
   * @param int $index
   *   The item's index.
   *
   * @return string
   *   The publication date of the item or an empty string.
   */
  public function getItemPublicationDate(int $index) {
    if (empty($this->items[$index])) {
      return '';
    }
    return (string) $this->items[$index]->pubDate ?? '';
  }

  /**
   * Get an item's audio's URI.
   *
   * @param int $index
   *   The item's index.
   *
   * @return string
   *   The audio file's URI of the item or an empty string.
   */
  public function getItemAudioUri(int $index) {
    if (empty($this->items[$index]) || !isset($this->items[$index]->enclosure)) {
      return '';
    }
    return (string) $this->items[$index]->enclosure['url'] ?? '';
  }

  /**
   * Get an item's audio's size.
   *
   * @param int $index
   *   The item's index.
   *
   * @return string
   *   The audio file's size in bytes of the item or an empty string.
   */
  public function getItemAudioSize(int $index) {
    if (empty($this->items[$index]) || !isset($this->items[$index]->enclosure)) {
      return '';
    }
    return (string) $this->items[$index]->enclosure['length'] ?? '';
  }

  /**
   * Get an item's audio type.
   *
   * @param int $index
   *   The item's index.
   *
   * @return string
   *   The audio file's MIME type of the item or an empty string.
   */
  public function getItemAudioType(int $index) {
    if (empty($this->items[$index]) || !isset($this->items[$index]->enclosure)) {
      return '';
    }
    return (string) $this->items[$index]->enclosure['type'] ?? '';
  }

  /**
   * Get an item's duration.
   *
   * @param int $index
   *   The item's index.
   *
   * @return string
   *   The duration of the item or an empty string.
   */
  public function getItemDuration(int $index) {
    if (empty($this->items[$index]) || empty($this->namespaces['itunes'])) {
      return '';
    }
    return (string) $this->items[$index]->children($this->namespaces['itunes'])->duration ?? '';
  }

}
