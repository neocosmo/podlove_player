(function ($, Drupal) {
  Drupal.behaviors.podlovePlayer = {
    attach: function (context, settings) {
      for (item of drupalSettings.podlovePlayer.podlovePlayer) {
        $(item.selector, context).once('podlovePlayer').each(function () {
          podlovePlayer(item.selector, item.episode, item.config);
        });
      }
    }
  };
})(jQuery, Drupal);
